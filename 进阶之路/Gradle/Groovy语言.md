## 常用操作

#### 输出语句

```groovy
println("输出内容")
```

#### 定义变量

```groovy
// def是弱类型变量修饰关键字
def num = 18;
```

#### 集合操作

```groovy
// 定义一个集合
def list = ['a','b']
// 往集合中添加元素
list << 'c'
// 取出list中的第三个元素
list.get(2)
```

#### Map操作

```groovy
// 定义一个Map
def map = ['k1':'v1','k2':'v2']
// 往Map中添加键值对
map.k3 = 'v3'
// 取出map中的值
map.get('k1')
```

## Groovy中的闭包

闭包其实就是一段代码块，在gradle中一般是把闭包当成参数来使用

#### 定义一个闭包

```groovy
def b = {
    v ->
    println "hello ${v}"
}
// 定义方法时，需要使用到闭包类型的参数
def method(Closure closure){
    closure("v是个参数")
}
// 调用方法
method(b)
```



**备注：**

1. groovy中可以省略末尾的分号
2. groovy中可以省略括号