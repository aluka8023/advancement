## Gradle工程目录

- .gradle目录是当前工程相关参数配置以及环境变量
- src目录与maven项目一致，为项目源码目录
- build.gradle文件为项目属性以及依赖配置文件
- settings.gradle文件为项目模块聚合信息配置文件

## build.gradle配置文件

#### group

​	表示当前项目组织名称

#### name(artifact)

​	表示当前项目名称

#### version

​	表示当前项目版本号

#### sourceCompatibility = 1.8

​	表示当前项目环境的源码版本

#### targetCompatibility = 1.8 

​	表示当前项目环境的编译版本

#### compileJava.options.encoding = 'UTF-8'

​	表示当前项目环境代码编码

#### compileTestJava.options.encoding = 'UTF-8'

​	表示当前项目环境测试代码编码

#### apply

​	表示应用插件

​	**apply plugin: 'java'**

​	表示当前项目使用的语言环境为java

​	**apply plugin: 'war'**

​	表示当前项目打包方式为war

#### dependencies

​	表示添加依赖jar包

​	每一个jar包的坐标都有三个基本元素组成

​	group、name、version

​	同时每一个jar包都需要添加作用域

​	testCompile 表示仅在测试的时候起作用

​	compile 表示在编译的时候起作用



#### repositories

​	表示当前项目使用的仓库路径

- 指定Maven中央仓库：mavenCentral()
- 指定Maven本地仓库：mavenLocal()  >> 使用当前环境变量 GRADLE_USER_HOME配置的本地仓库

#### allprojects

​	可用allprojects将原build.gradle配置属性进行闭包处理，即可将父属性传递给子模块

#### ext

​	自定义拓展属性