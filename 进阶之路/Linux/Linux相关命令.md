### 更改linux系统时间并同步至硬件

```shell
date -s "2020-11-13 09:03:00" && hwclock --systohc
```

### 查看某目录磁盘占用情况

```shell
# 查看当前目录下所有文件的总大小
du -sh
# 查看当前目录下文件夹磁盘使用大小情况 --max-depth=1表示当前1级子目录
du -h --max-depth=1
```

### 清除系统缓存

```shell
echo 3 > /proc/sys/vm/drop_caches 1 drop_caches
```

