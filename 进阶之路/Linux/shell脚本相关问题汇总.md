## 问题一：在Linux中执行.sh脚本，异常/bin/sh^M: bad interpreter: No such file or directory

> 这是不同系统编码格式引起的：在windows系统中编辑的.sh文件可能有不可见字符，所以在Linux系统下执行会报以上异常信息

解决办法：

```shell
# 1.通过vi编辑器打开对应脚本
vi xx.sh
# 2.查看当前脚本编码 如果出现 doc格式则表明脚本编码不符
:set ff
# 3.切换脚本编码，然后保存即可
:set ff=unix
```



### 清除系统缓存

echo 3 > /proc/sys/vm/drop_caches