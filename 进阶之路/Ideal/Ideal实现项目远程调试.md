## 使用IDEA进行远程调试



### 使用特定JVM参数运行服务端代码

要让远程服务器运行的代码支持远程调试，则启动的时候必须加上特定的JVM参数，这些参数是：

```shell
-Xdebug -Xrunjdwp:transport=dt_socket,suspend=n,server=y,address=${debug_port}
# 上述${debug_port}为自定义调试端口，只要不是常用端口即可
# 如果只是临时调试，在端口号前面不要加上限制访问的IP地址，调试完成之后，将上述JVM参数去除掉之后重新发布下，防范开放远程调试端口可能带来的安全风险。
```



### 本地连接远程服务器debug的端口

打开Intellij IDEA，在顶部靠右的地方选择”Edit Configurations…”，进去之后点击+号，选择”Remote”，按照下图的只是填写红框内的内容，其中Name填写名称，这里为`remote webserver`，host为远程代码运行的机器的ip/hostname，port为上一步指定的debug_port，本例是`5555`。然后点击Apply，最后点击OK即可

![img](https://upload-images.jianshu.io/upload_images/2636642-e58bb8ecfa436e26.png?imageMogr2/auto-orient/strip|imageView2/2)

### 本地IDEA启动debug模式

现在在上一步选择”Edit Configurations…”的下拉框的位置选择上一步创建的`remote webserver`，然后点击右边的`debug`按钮(长的像臭虫那个)，看控制台日志，如果出现类似“Connected to the target VM, address: ‘xx.xx.xx.xx:5555’, transport: ‘socket’”的字样，就表示连接成功过了。我这里实际显示的内容如下：

```
Connected to the target VM, address: '10.185.0.192:15555', transport: 'socket'
```

### 设置断点，开始调试

远程debug模式已经开启，现在可以在需要调试的代码中打断点了

现在在本地发送一个到远程服务器的请求，看本地控制台的bug界面，划到debugger这个标签，可以看到当前远程服务的内部状态（各种变量）已经全部显示出来了，并且在刚才设置了断点的地方，也显示了该行的变量值。



> 需要注意的是，用于远程debug的代码必须与远程部署的代码完全一致，不能发生任何的修改，否则打上的断点将无法命中，切记