### 配置忽略属性

```
svn propset svn:ignore ".settings
target
.classpath
.project
*.iml" .
```

### 查看设置属性

```shell
svn plist
svn:ignore
```

### 查看设置内容

```
svn pget svn:ignore
.settings
target
.classpath
.project
```