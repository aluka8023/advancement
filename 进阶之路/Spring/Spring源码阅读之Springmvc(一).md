# Springmvc

### 搭建springmvc源码学习工程 >> learning-springmvc

新建项目子模块module：learning-springmvc

编辑build.gradle配置文件，引入spring-webmvc依赖，由于需要tomcat启动，所以一并加入tomcat依赖

```js
plugins {
    id 'java'
}

group 'org.springframework'
version '5.0.19.BUILD-SNAPSHOT'

sourceCompatibility = 1.8

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    compile(project(":spring-webmvc"))
    compile 'org.apache.tomcat.embed:tomcat-embed-core:8.5.32'
    testCompile group: 'junit', name: 'junit', version: '4.12'
}

```

增加boot项目配置类，用于Spring Bean扫描

```java
package com.aluka.learning.springmvc.config;

import org.springframework.context.annotation.ComponentScan;

/**
 * @author gongli
 * @version 1.0
 * @since 9:45 2020/9/23
 */
@ComponentScan("com.aluka.learning")
public class LearnConfiguration {
}
```

增加Controller类，对外提供访问测试地址

```java
package com.aluka.learning.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author gongli
 * @version 1.0
 * @since 9:27 2020/9/23
 */
@Controller
public class LearnMvcController {

	@GetMapping("/")
	@ResponseBody
	public String index(){
		return "/index";
	}
}

```

编写测试函数，测试spring-boot整合tomcat启动，并加载当前工程下提供的Bean

```java
package com.aluka.learning;

import com.aluka.learning.springmvc.config.LearnConfiguration;
import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;
import org.junit.Test;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletException;

/**
 * @author gongli
 * @version 1.0
 * @since 9:47 2020/9/23
 */
public class SpringmvcTest {

	@Test
	public void contextTest() throws ServletException {
		AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();
		ctx.register(LearnConfiguration.class);
		ctx.refresh();

		// Create and register the DispatcherServlet
		DispatcherServlet servlet = new DispatcherServlet(ctx);
		Tomcat tomcat = new Tomcat();
		tomcat.setPort(8008);
		Context context = tomcat.addContext("/learning", null);
		Tomcat.addServlet(context, "DispatcherServlet", servlet);
		context.addServletMappingDecoded("/*", "DispatcherServlet");
		try{
			tomcat.init();
			tomcat.start();
			tomcat.getServer().await();
		}catch (Exception e){
			e.printStackTrace();
		}


	}
}

```

