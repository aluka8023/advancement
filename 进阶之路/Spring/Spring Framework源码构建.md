### spring framework源码下载

[spring framework 5.0]: https://github.com/spring-projects/spring-framework

[gradle 6.5]: https://services.gradle.org/distributions/

#### 1. 安装配置gradle

​	**配置环境变量**

​	在系统变量中新增GRADLE_HOME = gradle解压目录

​	在系统变量中新增GRADLE_USER_HOME = gradle仓库目录

​	在path中新增 %GRADLE_HOME%\bin



#### 问题一：The build scan plugin is not compatible with this version of Gradle！

​	原因是gradle版本过高，更换至4.1即可



#### 问题二：源码报错 找不到符号 DefaultNamingPolicy，Objenesis，InstantiatorStrategy，ObjectInstantiator等

​	原因是spring为了避免第三方class冲突，spring把最新的cglib和objenesis重新打包jar并没有放在源码里

​	解决办法是 通过gradle 执行 objenesisRepackJar 和 cglibRepackJar

1. 通过cmd进入spring源码根目录下
2. 执行 gradle objenesisRepackJar 和 gradle cglibRepackJar 命令