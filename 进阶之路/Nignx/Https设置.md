### 1. nginx 的 ssl 模块安装

查看 nginx 是否安装 `http_ssl_module` 模块。

```shell
/usr/local/nginx/sbin/nginx -V
```

如果出现 `configure arguments: --with-http_ssl_module`, 则已安装

如果未安装，则需要安装ssl模块

```shell
# 进入nginx目录
# 配置ssl模块
./configure --prefix=/usr/local/nginx --with-http_ssl_module
```

- 使用 `make` 命令编译（**使用`make install`会重新安装nginx**），此时当前目录会出现 `objs` 文件夹。

- 用新的 nginx 文件覆盖当前的 nginx 文件。

  ```shell
   cp ./objs/nginx /usr/local/nginx/sbin/
  ```

  再次查看安装的模块（`configure arguments: --with-http_ssl_module`说明ssl模块已安装）

### 2. ssl 证书部署

- 下载申请好的 ssl 证书文件压缩包到本地并解压（这里是用的 pem 与 key 文件，文件名可以更改）。
- 在 nginx 目录新建 cert 文件夹存放证书文件。
- 将这两个文件上传至服务器的 cert 目录里。

### 3. nginx.conf 配置

配置 https server。
**注释掉之前的 http server 配置**，新增 https server

```json
server {
    # 服务器端口使用443，开启ssl, 这里ssl就是上面安装的ssl模块
    listen       443 ssl;
    # 域名，多个以空格分开
    server_name  baidu.com www.baidu.com;
	# ssl证书地址
	ssl_certificate /usr/local/nginx/cert/ssl.pem
    # pem文件的路径
	ssl_certificate_key /usr/local/nginx/cert/ssl.key;
    # key文件的路径
    
	# ssl验证相关配置
    
    #缓存有效期
	ssl_session_timeout 5m
    #加密算法
	ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:ECDHE:ECDH:AES:HIGH:!NULL:!aNULL:!MD5:!ADH:!RC4;
    #安全链接可选的加密协议
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2 ;	
    #使用服务器端的首选算法
	ssl_prefer_server_ciphers on
    
	location / {
    	root html;
    	index index.html index.htm;
	}
}
```

**将 http 重定向 https**

```
server {
    listen       80;
    server_name  baidu.com www.baidu.com;
    return 301 https://$server_name$request_uri;
}
```

