## docker search

> 用于查询官方镜像

![image-20201118104328389](C:\Users\Aluka\AppData\Roaming\Typora\typora-user-images\image-20201118104328389.png)

如果想细看具体镜像的版本可使用如下脚本进行查询

```shell
#!/bin/sh
#
# Simple script that will display docker repository tags.
#
# Usage:
#   $ docker-show-repo-tags.sh ubuntu centos
for Repo in $* ; do
  curl -s -S "https://registry.hub.docker.com/v2/repositories/library/$Repo/tags/" | \
    sed -e 's/,/,\n/g' -e 's/\[/\[\n/g' | \
    grep '"name"' | \
    awk -F\" '{print $4;}' | \
    sort -fu | \
    sed -e "s/^/${Repo}:/"
done
```

![image-20201118104743493](C:\Users\Aluka\AppData\Roaming\Typora\typora-user-images\image-20201118104743493.png)

## docker images

查看docker所有本地镜像

## docker ps

查看docker所有容器进程信息