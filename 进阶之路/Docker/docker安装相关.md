# 前言

从 2017 年 3 月开始 docker 在原来的基础上分为两个分支版本: Docker CE 和 Docker EE。

Docker CE 即社区免费版，Docker EE 即企业版，强调安全，但需付费使用。

Docker 官方建议要 CentOS7.0 及以上系统版本，本文介绍 Docker CE 在CentOS下的安装使用。

 

 

# 1.前提条件

## 1.1.内核

 Docker运行对内核要求比较高，因此一般建议直接在Ubuntu这样的平台运行。但作为一个容器标准，Docker也是支持其他如CentOS, Mac OS X, Windows等平台。目前Docker支持以下版本CentOS:

- CentOS 7(64位)
- CentOS 6.5(64位)及以后

在运行CentOS 6.5及以后版本时，需要内核版本>=2.6.32-431，因为这些内核包含了运行Docker的一些特定修改。

```
$ uname -r
2.6.32-431.17.1.el6.x86_64
```

注：网上很多教程说CentOS6.5必须要升级内核到3.10才能使用docker，其实是【可选】升级，但最好升级。

 

## 1.2.Device Mapper

Docker默认使用AUFS作为存储驱动，但是AUFS并没有被包括在Linux的主线内核中。CentOS中可以使用Device Mapper作为存储驱动，这是在2.6.9内核版本引入的新功能。我们需要先确认是否启用该功能:

```
$ ls -l /sys/class/misc/device-mapper
lrwxrwxrwx 1 root root 0 May  1 20:55 /sys/class/misc/device-mapper -> ../../devices/virtual/misc/device-mapper
```

如果没有检测到Device Mapper，需要安装device-mapper软件包:

```
$ sudo yum install -y device-mapper
```

然后重新加载dm_mod内核模块:

```
$ sudo modprobe dm_mod
```

 

 

# 2.安装

## 2.1.CentOS 7

### 2.2.1.准备

- #### **CentOS7能上外网**

- #### **yum -y install gcc**

- #### **yum -y install gcc-c++**

![img](https://img2018.cnblogs.com/i-beta/1577453/202001/1577453-20200119164532261-468748089.png)

- #### **卸载旧版本**

```
yum remove docker \ docker-client \ docker-client-latest \ docker-common \ docker-latest \ docker-latest-logrotate \ docker-logrotate \ docker-selinux \ docker-engine-selinux \ docker-engine
```

![img](https://img2018.cnblogs.com/i-beta/1577453/202001/1577453-20200119164426179-1708224927.png)

如果 yum 报告未安装任何这些软件包，这表示情况正常。

　　

### 2.2.2.安装

- #### **安装依赖包**

```
yum install -y yum-utils device-mapper-persistent-data lvm2
```

- #### **设置stable镜像仓库**

```
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
或
yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
```

**![img](https://img2018.cnblogs.com/i-beta/1577453/202001/1577453-20200119174614479-1162070262.png)**

- #### **更新yum软件包索引**

```
yum makecache fast
```

![img](https://img2018.cnblogs.com/i-beta/1577453/202001/1577453-20200119164249729-569964462.png)

如果这是自添加 Docker 镜像仓库以来您首次刷新软件包索引，系统将提示您接受 GPG 密钥，并且将显示此密钥的指纹。验证指纹是否正确，并且在正确的情况下接受此密钥。指纹应匹配 060A 61C5 1B55 8A7F 742B 77AA C52F EB6B 621E 9F35。

- #### **安装docker**

1、安装指定版本：

```
yum list docker-ce.x86_64  --showduplicates | sort -r    #从高到低列出Docker-ce的版本
```

![img](https://img2018.cnblogs.com/i-beta/1577453/202001/1577453-20200119164154466-619929091.png)

该软件包名称是软件包名称（docker-ce）加上版本字符串（第二列），从第一个冒号（:）一直到第一个连字符，并用连字符（-）分隔。

例如：指定版本（docker-ce-18.09.9）进行安装：

```
yum install docker-ce-<VERSION_STRING> docker-ce-cli-<VERSION_STRING> containerd.io

# 例如：
yum install docker-ce-18.09.9 docker-ce-cli-18.09.9 containerd.io
```

![img](https://img2018.cnblogs.com/i-beta/1577453/202001/1577453-20200119164646113-254004212.png)

2、安装最新版本：

```
yum -y install docker-ce
```

- #### **启动docker**

执行以下命令启动docker：

```
systemctl start docker
```

- #### **测试docker**

![img](https://img2018.cnblogs.com/i-beta/1577453/202001/1577453-20200119170204575-978724570.png)

 

 

## 2.2.CentOS 6.5

### 2.2.1.准备

- #### **禁用selinux [可选]**

由于Selinux和LXC有冲突,所以需要禁用selinux.编辑/etc/selinux/config,设置两个关键变量.

```
SELINUX=disabled
SELINUXTYPE=targeted
```

注：网上有教程说要设置，但我建议不要设置，因为会影响安全性

- #### **安装Fedora EPEL源**

```
yum install http://ftp.riken.jp/Linux/fedora/epel/6/x86_64/epel-release-6-8.noarch.rpm
```

**![img](https://img2018.cnblogs.com/i-beta/1577453/202001/1577453-20200114093244168-1268430633.png)**

- #### **添加hop5.repo源**

```
cd /etc/yum.repos.d
wget http://www.hop5.in/yum/el6/hop5.repo
```

 

### 2.2.2.yum安装

- #### **升级带aufs模块的3.10内核【可选】**

```
yum install kernel-ml-aufs kernel-ml-aufs-devel
```

修改grub的主配置文件/etc/grub.conf，设置default=0，表示第一个title下的内容为默认启动的kernel（一般新安装的内核在第一个位置）：

![img](https://img2018.cnblogs.com/i-beta/1577453/202001/1577453-20200113174702597-320102472.png)

重启系统 `reboot now，`然后执行以下命令查看是否已经是3.10内核：

![img](https://img2018.cnblogs.com/i-beta/1577453/202001/1577453-20200113174757736-1382509719.png)

查看内核是否支持aufs：

![img](https://img2018.cnblogs.com/i-beta/1577453/202001/1577453-20200113174833758-986811323.png)

- #### **安装docker**

安装依赖：

```
yum install redhat-lsb　　　　　　　　
yum install device-mapper-libs
yum install libcgroup*
```

如出现以下错误：

![img](https://img2018.cnblogs.com/i-beta/1577453/202001/1577453-20200114094934028-1020573124.png)

解决办法是编辑/etc/yum.repos.d/epel.repo，把基础的恢复(baseurl)，镜像(mirrorlist)的地址注释掉：

![img](https://img2018.cnblogs.com/i-beta/1577453/202001/1577453-20200114092157003-225275654.png)

 

安装docker：

```
yum install docker-io
```

如出现以下报错：

```
Error: Cannot retrieve metalink for repository: epel. Please verify its path and try again
```

解决办法是编辑/etc/yum.repos.d/epel.repo，把基础的恢复(baseurl)，镜像(mirrorlist)的地址注释掉：

![img](https://img2018.cnblogs.com/i-beta/1577453/202001/1577453-20200114092157003-225275654.png)

如出现如下错误：

![img](https://img2018.cnblogs.com/i-beta/1577453/202001/1577453-20200114095831855-1290158604.png)

原因是：网络问题，下载失败。解决办法：则执行多几次yum install docker-io命令，直至下载并安装成功（我这里耗时1个多小时）。

继续安装，如出现以下信息，则说明能正常安装：

![img](https://img2018.cnblogs.com/i-beta/1577453/202001/1577453-20200114092318658-1691151292.png)

- #### **启动docker**

执行以下命令启动docker：

```
service docker start
```

![img](https://img2018.cnblogs.com/i-beta/1577453/202001/1577453-20200113175155258-471530564.png)

如出现以上提示则说明安装并启动成功。

如出现以下提示则说明安装失败：

![img](https://img2018.cnblogs.com/i-beta/1577453/202001/1577453-20200114092807640-1774825027.png)

解决方法：执行以下命令删除docker-io

```
sudo yum remove docker-io
```

![img](https://img2018.cnblogs.com/i-beta/1577453/202001/1577453-20200114103817835-1904818974.png)

执行以下命令手工安装docker：

```
rpm -ivh ./docker-engine-1.7.1-1.el6.x86_64.rpm
```

![img](https://img2018.cnblogs.com/i-beta/1577453/202001/1577453-20200114104104465-1876041530.png)

注：安装包下载地址：https://yum.dockerproject.org/repo/main/centos/6/Packages/

再次执行以下命令启动docker：

```
service docker start
```

![img](https://img2018.cnblogs.com/i-beta/1577453/202001/1577453-20200114104208699-600629635.png)

【可选】设置在服务器启动时启动：

```
chkconfig docker on
```

 

 

# 3.使用

-  查看docker版本

```
docker version
```

![img](https://img2018.cnblogs.com/i-beta/1577453/202001/1577453-20200113175240768-1448772268.png)

-  查看docker日志

```
cat /var/log/docker
```

![img](https://img2018.cnblogs.com/i-beta/1577453/202001/1577453-20200113175345330-1687159210.png)

- 搜索镜像

```
docker search tomcat
```

![img](https://img2018.cnblogs.com/i-beta/1577453/202001/1577453-20200114112520268-145617052.png)

 注：start代表受欢迎程度。OFFICIAL代表官方版本

- 查看当前所有镜像

```
docker images
```

- 下载镜像

```
docker pull centos
```

- 运行容器

```
docker run centos echo "hello word"
```

- *运行容器 hello word*

```
docker run hello-world
```

![img](https://img2018.cnblogs.com/i-beta/1577453/202001/1577453-20200119171711943-236059458.png)

输出这段提示以后，hello world 就会停止运行，容器自动终止。

 

 

# 4.删除

可以使用yum来删除docker

- #### **查询删除docker**

1.列出docker包的具体的名字

```
$ sudo yum list  installed | grep docker
containerd.io.x86_64                 1.2.4-3.1.el7 
docker-ce.x86_64                     3:18.09.3-3.el7
docker-ce-cli.x86_64                 1:18.09.3-3.el7
```

2.删除docker

```
$ sudo yum -y remove containerd.io.x86_64 \
                     docker-ce.x86_64 \
                     docker-ce-cli.x86_64  
```

- #### **直接删除docker**

[![复制代码](https:////common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
sudo yum remove docker \
                  docker-io \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
```

[![复制代码](https:////common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

备注：以上命令只会删除docker运行环境，并不会删除镜像，容器，卷文件，以及用户创建的配置文件。

清除镜像和容器文件：

```
sudo rm -rf /var/lib/docker
```

手动查找并删除用户创建的配置文件。

 

 

# 5.注意

## **5.1.手动挂载cgroup**

低版本的Redhat(6.3)可能要手动挂载cgroup，具体操作方法如下：

- 首选禁用cgroup对应服务cgconfig

```
 service cgconfig stop # 关闭服务 
 chkconfig cgconfig off # 取消开机启动
```

- 然后挂载cgroup,可以命令行挂载

```
mount -t cgroup none /cgroup  #仅本次有效
```

- 或者修改配置文件,编辑`/etc/fstab`,加入

```
none                    /cgroup                 cgroup  defaults        0 0
```

![img](https://img2018.cnblogs.com/i-beta/1577453/202001/1577453-20200114105456092-1793581430.png)

 

## **5.2.以非root用户管理docker**

Docker守护程序绑定到Unix socket而不是TCP端口。默认情况下，Unix socke是root用户才有，而其他用户只能通过使用sudo使用它。Docker守护程序始终以root``用户身份运行。

如果您不想在docker``命令前加上sudo``，请创建一个名为docker的Unix组``并向其添加用户。当Docker守护程序启动时，它会创建一个可由该docker``组成员访问的Unix socket。

- 1.创建docker组

```
sudo groupadd docker
```

- 2.添加user到docker组

```
sudo usermod -aG docker $USER
```

请将$USER替换为系统中你的除root外的某个用户。

- 3.注销并重新登录，以便重新评估您的组成员身份。

如果在虚拟机上进行测试，则可能需要重新启动虚拟机才能使更改生效。

- 4.验证是否可以通过不使用sudo运行docker命令。

```
 docker run hello-world
```

 

如果最初在添加用户到docker组之前，使用的是sudo的Dokcer CLI命令。你可能会出现如下错误，表明你的~/.docker目录由于sudo命令而创建的权限不正确。

```
WARNING: Error loading config file: /home/user/.docker/config.json -
stat /home/user/.docker/config.json: permission denied
```

要解决此问题，请删除~/.docker/目录（它会自动重新创建，但任何自定义设置都会丢失），或使用以下命令更改其所有权和权限：

```
$ sudo chown "$USER":"$USER" /home/"$USER"/.docker -R
$ sudo chmod g+rwx "$HOME/.docker" -R
```

 

## 5.3.镜像加速

鉴于国内网络问题，后续拉取 Docker 镜像十分缓慢，我们可以需要配置加速器来解决。

Docker国内镜像：

- 网易加速器：http://hub-mirror.c.163.com
- 官方中国加速器：https://registry.docker-cn.com
- ustc的镜像：https://docker.mirrors.ustc.edu.cn

也可使用私人镜像加速器地址，如使用阿里云的镜像加速器：登录阿里云->产品->搜索"容器镜像服务"->镜像库->镜像加速器。

新版的 Docker 使用 /etc/docker/daemon.json（Linux） 来配置 Daemon。

请在该配置文件中加入（没有该文件的话，请先建一个）：

```
vi /etc/docker/daemon.json 
{ 
　　"registry-mirrors": ["https://hub-mirror.c.163.com"] 
} 
```

配置成功后，CentOS6.5重启：

```
service docker restart
```

配置成功后，CentOS7重启：

```
systemctl daemon-reload     #重启加速配置文件
systemctl restart docker    #重启docker后台服务
```