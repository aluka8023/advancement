**介绍**
 我的系统是windows10的操作系统，首先安装[docker for windows](https://www.docker.com/products/docker-desktop)，为什么要在windows上安装docker呢？因为docker实在太方便了，用到什么软件只需要下载它的镜像，安装、启动镜像，就可以直接使用了，一般不需要你配置环境参数，比如Mysql、redis，开发人员应该都会安装吧！这里我就不在介绍如何安装docker for windows了，[百度](https://www.baidu.com/s?ie=utf-8&f=8&rsv_bp=0&rsv_idx=1&tn=baidu&wd=docker for windows 安装教程&rsv_pq=e1e261bf0004ccd6&rsv_t=0a95hrC%2B5SCTCUkyGtbsmBZHrf2BlKiFpIxzbrMJgz7R%2Fd9HCGNXpxFTKF4&rqlang=cn&rsv_enter=1&rsv_sug3=38&rsv_sug1=21&rsv_sug7=101&sug=docker%20for%20windows&rsv_n=1)有很多教程，但是有点需要注意的是，有的电脑需要打开cpu虚拟化配置。**在最后面介绍nacos容器如何配置连接本地的数据库。**

**下载镜像**
 打开[Windows PowerShell](https://jingyan.baidu.com/article/afd8f4debdd2f434e386e951.html)，首先试一下你的docker for windows是否安装成功：
 `docker search nacos`

![img](https://user-gold-cdn.xitu.io/2019/3/2/1693c61135a3e05f?imageView2/0/w/1280/h/960/format/webp/ignore-error/1)

如果可以查到那就没有问题，有问题的话请根据提示内容自行百度，下面开始安装nacos，执行命令：
`docker pull nacos/nacos-server`
 静静的等待上几分钟，安装成功之后现在会有一串字符码。查看是否安装成功：
`docker images`

![img](https://user-gold-cdn.xitu.io/2019/3/2/1693c6c6d51f8dc7?imageView2/0/w/1280/h/960/format/webp/ignore-error/1)

如果查到了那么恭喜你已经安装成功了，距离成功又进了一步！



**启动镜像**
 这里启动容器的时候参数配置我就不在详解了，不明白的话，评论区留言，有不会的问题一定要及时询问，期待你的评论呦！

```shell
docker run -d \
-e MODE=standalone \
-e SPRING_DATASOURCE_PLATFORM=mysql \
-e MYSQL_SERVICE_HOST=172.17.0.2 \
-e MYSQL_SERVICE_PORT=3306 \
-e MYSQL_SERVICE_USER=root \
-e MYSQL_SERVICE_PASSWORD=123456 \
-e MYSQL_SERVICE_DB_NAME=nacos_config \
-p 8848:8848 \
--restart=always \
--name nacos \
nacos/nacos-server

#ip 是你的mysql容器的ip（不要写成127.0.0.1）

查询方式：docker inspect mysql | grep IPAddress 查询出来写上去即可
```

![img](https://user-gold-cdn.xitu.io/2019/3/2/1693c71b1aff67d4?imageView2/0/w/1280/h/960/format/webp/ignore-error/1)

这里我就不在执行了因为我已经安装过了，如何这一步有问题的话评论留言，我会及时回复的。查看是否启动成功：
`docker ps`

![img](https://user-gold-cdn.xitu.io/2019/3/2/1693c74a1b56c328?imageView2/0/w/1280/h/960/format/webp/ignore-error/1)

如果可以做到这一步，你已经很棒了！距离成功已经不远了，那么再坚持一下吧！打开[nacos](http://localhost:8848/nacos/index.html)

![img](https://user-gold-cdn.xitu.io/2019/3/2/1693c83fb4be7d4a?imageView2/0/w/1280/h/960/format/webp/ignore-error/1)

nacos的默认端口号是8848，珠穆拉玛发的高度也是8848米呦！默认账号密码是nacos/nacos。至此安装已经结束。



**配置本地数据库**
 [nacos数据库脚本](https://github.com/alibaba/nacos/blob/master/config/src/main/resources/META-INF/nacos-db.sql)别忘记执行呦。 要想配置连接你本地的数据库，首先你要在你的电脑上安装mysql，我的实在docker for windows中安装的mysql版本是5.7，nacos官方要求数据库也必须是5.6+的。进入容器：
 `docker exec -it <容器ID> bash`

![img](https://user-gold-cdn.xitu.io/2019/3/2/1693c94e7cd5a0ea?imageView2/0/w/1280/h/960/format/webp/ignore-error/1)

进入成功，之后打开config/application.properties

![img](https://user-gold-cdn.xitu.io/2019/3/2/1693c964e8760c35?imageView2/0/w/1280/h/960/format/webp/ignore-error/1)

![img](https://user-gold-cdn.xitu.io/2019/3/2/1693c9e94e39674e?imageView2/0/w/1280/h/960/format/webp/ignore-error/1)

需要修改的内容就是我用红色框圈起来的，可以很明显的看出来这块是key-value方式的配置，大家只需要根据自己的环境来配置就好了，一定要仔细的检查一边呦，host、dbName、username、password这几个参数一定要注意呦，还有一点呢我把db.url.1这段配置给他注释掉了，因为我的电脑上只有一个mysql节点，没有从节点，所以不需要配置了。保存退出shift + : + w + q。重启nacos：
`docker restart <容器name>`

![img](https://user-gold-cdn.xitu.io/2019/3/2/1693ca222f19b52a?imageView2/0/w/1280/h/960/format/webp/ignore-error/1)

看好呦我现在没有在容器内部。现在打开nacos，随便新建一个配置，查看查看数据库是否有这条数据呢！！！

![img](https://user-gold-cdn.xitu.io/2019/3/2/1693ca4718a0a03a?imageView2/0/w/1280/h/960/format/webp/ignore-error/1)

![img](https://user-gold-cdn.xitu.io/2019/3/2/1693ca481791044d?imageView2/0/w/1280/h/960/format/webp/ignore-error/1)


